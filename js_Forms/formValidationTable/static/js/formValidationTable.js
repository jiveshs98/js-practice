
function fun(){
    var firstName= frm.fname.value;
    console.log(firstName);
    var lastName= frm.lname.value;
    var genderDivValue= frm.gender.value;
    var contactValue= frm.contact.value;
    var dayValue= frm.day.value;
    var monthValue= frm.month.value;
    var yearValue= frm.year.value;
    var emailValue= frm.email.value;
    var passWord= frm.pass.value;
    var confirmPassword= frm.confirmPass.value;


    // Validating first name
    if(firstName==""){
        document.getElementById("fnameErr").style.color="red";
        document.getElementById("fnameErr").innerHTML="*This field is required";
        return false;
    }
    else if(!firstName.match(/^([a-z A-Z])*$/)){
        document.getElementById("fnameErr").style.color="red";
        document.getElementById("fnameErr").innerHTML="*Use only alphabets";
        return false;
    }
    else{
        document.getElementById("fnameErr").innerHTML="";
        
    }

    // Validating last name
    if(lastName==""){
        document.getElementById("lnameErr").style.color="red";
        document.getElementById("lnameErr").innerHTML="*This field is required";
        return false;
    }
    else if(!lastName.match(/^([a-z A-Z])*$/)){
        document.getElementById("lnameErr").style.color="red";
        document.getElementById("lnameErr").innerHTML="*Use only alphabets";
        return false;
    }
    else{
        document.getElementById("lnameErr").innerHTML="";
    }

    // Validating gender
    if(genderDivValue=="M" || genderDivValue=="F" || genderDivValue=="O"){
        document.getElementById("genderDivErr").innerHTML="";
    }
    else{
        document.getElementById("genderDivErr").style.color="red";
        document.getElementById("genderDivErr").innerHTML="*This field is required";
        return false;
    }

    // Validating contact
    if(contactValue==""){
        document.getElementById("contactErr").style.color="red";
        document.getElementById("contactErr").innerHTML="*This field is required";
        return false;
    }
    else if(!contactValue.match(/^([0-9])*$/)){
        document.getElementById("contactErr").style.color="red";
        document.getElementById("contactErr").innerHTML="*Use only digits";
        return false;
    }
    else if(contactValue.length!=10){
        document.getElementById("contactErr").style.color="red";
        document.getElementById("contactErr").innerHTML="*Should be a 10-digit no."
        return false;
    }
    else{
        document.getElementById("contactErr").innerHTML="";
    }

    // Validating DOB
    if(dayValue==-1 || monthValue==-1 || yearValue==-1){
        document.getElementById("dobErr").style.color="red";
        document.getElementById("dobErr").innerHTML= "*This Field is required";
        return false;
    }
    else{
        document.getElementById("dobErr").innerHTML= "";
    }

    // Validating Email
    if(emailValue==""){
        document.getElementById("emailErr").style.color="red";
        document.getElementById("emailErr").innerHTML="*This field is required";
        return false;
    }
    else if(!emailValue.match(/^([a-zA-Z0-9\_\-\.])+@(([a-zA-Z0-9])+\.).*([a-zA-Z])$/)){
        document.getElementById("emailErr").style.color="red";
        document.getElementById("emailErr").innerHTML="*E-mail id not valid";
        return false;
    }
    else{
        document.getElementById("emailErr").innerHTML="";
    }

    // Validating Password
    if(passWord==""){
        document.getElementById("passErr").style.color="red";
        document.getElementById("passErr").innerHTML="*This field is required";
        return false;
    }

    else if(passWord.length<6){
        document.getElementById("passErr").style.color="red";
        document.getElementById("passErr").innerHTML="*Password must contain atleast 6 characters";
        return false;
    }

    else{
        document.getElementById("passErr").innerHTML="";
    }

    // Validating Confirm Password
    if(confirmPassword==""){
        document.getElementById("confirmPassErr").style.color="red";
        document.getElementById("confirmPassErr").innerHTML="*This field is required";
        return false;
    }
    else if(confirmPassword!=passWord){
        document.getElementById("confirmPassErr").style.color="red";
        document.getElementById("confirmPassErr").innerHTML="*Passwords do not match!!";
        return false;
    }
    else{
        document.getElementById("confirmPassErr").innerHTML="";
    }

    // var firstName= frm.fname.value;
    // console.log(firstName);
    // var lastName= frm.lname.value;
    // var genderDivValue= frm.gender.value;
    // var contactValue= frm.contact.value;
    // var dayValue= frm.day.value;
    // var monthValue= frm.month.value;
    // var yearValue= frm.year.value;
    // var emailValue= frm.email.value;
    // var passWord= frm.pass.value;
    // var confirmPassword= frm.confirmPass.value;
    var dob=dayValue+"/"+monthValue+"/"+yearValue;
    arr=[firstName,lastName,genderDivValue,contactValue,dob,emailValue,confirmPassword]; 
    result(arr);
    return false;
    
    
}

function result(x){

    /*
    for(var i=0;i<x.length;i++){
        var resultTag= document.createElement("p");
        resultTag.innerHTML= x[i];
        document.getElementById("result").appendChild(resultTag);
    }
    */
    for(var i=0;i<x.length;i++){
        document.write("<p>"+x[i]+"</p>");
    }
}

