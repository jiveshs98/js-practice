/*

function fun(){
    var a= frm.name.value;
    var b= frm.add.value;
    var c= frm.email.value;
    var d= frm.cont.value;
    var e= frm.gender.value;
    var f= frm.day.value;
    var g= frm.month.value;
    var h= frm.year.value;
    var i= frm.pass.value;
    if(a==""){
        document.getElementById("errnm").innerHTML="&#160;&#160;"+"This field is required";
        return false;
    }
    else if(!a.match(/^([a-z A-Z])*$/)){
        document.getElementById("errnm").innerHTML="&#160;&#160;"+"Use only letters";
        return false;
    }
    else{
        document.getElementById("errnm").innerHTML="";
    }

    if(b==""){
        document.getElementById("erradd").innerHTML="&#160;&#160;"+"This field is required";
        return false;
    }

    else{
        document.getElementById("erradd").innerHTML="";
    }

    if(d==""){
        document.getElementById("errcont").innerHTML="&#160;&#160;"+"This field is required";
        return false;
    }
    else if(!d.match(/^([0-9])*$/)){
        document.getElementById("errcont").innerHTML="&#160;&#160;"+"Use only digits";
        return false;
    }
    else if(d.length!=10){
        document.getElementById("errcont").innerHTML="&#160;&#160;"+"Should be a 10-digit no."
    }
    else{
        document.getElementById("errcont").innerHTML="";
    }

    if(e=="M" || e=="F"){
        document.getElementById("errgen").innerHTML="";
    }
    else{
        document.getElementById("errgen").innerHTML="&#160;&#160;"+"This field is required";
        return false;
    }



    if(f=="-1" || g=="-1" || h=="-1"){
        document.getElementById("errdob").innerHTML="&#160;&#160;"+"This field is required";
        return false;
    }
    else{
        document.getElementById("errdob").innerHTML="";
    }

    if(c==""){
        document.getElementById("errem").innerHTML="&#160;&#160;"+"This field is required";
        return false;
    }
    else if(!c.match(/^([a-zA-Z0-9\_\-\.])+@(([a-zA-Z0-9])+\.).*([a-zA-Z])$/)){
        document.getElementById("errem").innerHTML="&#160;&#160;"+"E-mail id not valid";
        return false;
    }
    else{
        document.getElementById("errem").innerHTML="";
    }

    if(i==""){
        document.getElementById("errpass").innerHTML="&#160;&#160;"+"This field is required";
        return false;
    }

    else if(i.length<6){
        document.getElementById("errpass").innerHTML="&#160;&#160;"+"Password must contain atleast 6 characters";
        return false;
    }

    else{
        document.getElementById("errpass").innerHTML="";
    }
}

*/


// var firstName= frm.fname.value;
// var lastName= frm.lname.value;
// var genderDivValue= frm.gender.value;
// var contactValue= frm.contact.value;
// var dayValue= frm.day.value;
// var monthValue= frm.month.value;
// var yearValue= frm.year.value;
// var emailValue= frm.e-mail.value;
// var passWord= frm.pass.value;
// var confirmPassword= frm.confirmPass.value;
function fun(){
    var firstName= frm.fname.value;
    console.log(firstName);
    var lastName= frm.lname.value;
    var genderDivValue= frm.gender.value;
    var contactValue= frm.contact.value;
    var dayValue= frm.day.value;
    var monthValue= frm.month.value;
    var yearValue= frm.year.value;
    var emailValue= frm.email.value;
    var passWord= frm.pass.value;
    var confirmPassword= frm.confirmPass.value;


    // Validating first name
    if(firstName==""){
        document.getElementById("fnameErr").style.color="red";
        document.getElementById("fnameErr").innerHTML="*This field is required";
        return false;
    }
    else if(!firstName.match(/^([a-z A-Z])*$/)){
        document.getElementById("fnameErr").style.color="red";
        document.getElementById("fnameErr").innerHTML="*Use only alphabets";
        return false;
    }
    else{
        document.getElementById("fnameErr").innerHTML="";
    }

    // Validating last name
    if(lastName==""){
        document.getElementById("lnameErr").style.color="red";
        document.getElementById("lnameErr").innerHTML="*This field is required";
        return false;
    }
    else if(!lastName.match(/^([a-z A-Z])*$/)){
        document.getElementById("lnameErr").style.color="red";
        document.getElementById("lnameErr").innerHTML="*Use only alphabets";
        return false;
    }
    else{
        document.getElementById("lnameErr").innerHTML="";
    }

    // Validating gender
    if(genderDivValue=="M" || genderDivValue=="F" || genderDivValue=="O"){
        document.getElementById("genderDivErr").innerHTML="";
    }
    else{
        document.getElementById("genderDivErr").style.color="red";
        document.getElementById("genderDivErr").innerHTML="*This field is required";
        return false;
    }

    // Validating contact
    if(contactValue==""){
        document.getElementById("contactErr").style.color="red";
        document.getElementById("contactErr").innerHTML="*This field is required";
        return false;
    }
    else if(!contactValue.match(/^([0-9])*$/)){
        document.getElementById("contactErr").style.color="red";
        document.getElementById("contactErr").innerHTML="*Use only digits";
        return false;
    }
    else if(contactValue.length!=10){
        document.getElementById("contactErr").style.color="red";
        document.getElementById("contactErr").innerHTML="*Should be a 10-digit no."
        return false;
    }
    else{
        document.getElementById("contactErr").innerHTML="";
    }

    // Validating DOB
    if(dayValue==-1 || monthValue==-1 || yearValue==-1){
        document.getElementById("dobErr").style.color="red";
        document.getElementById("dobErr").innerHTML= "*This Field is required";
        return false;
    }
    else{
        document.getElementById("dobErr").innerHTML= "";
    }

    // Validating Email
    if(emailValue==""){
        document.getElementById("emailErr").style.color="red";
        document.getElementById("emailErr").innerHTML="*This field is required";
        return false;
    }
    else if(!emailValue.match(/^([a-zA-Z0-9\_\-\.])+@(([a-zA-Z0-9])+\.).*([a-zA-Z])$/)){
        document.getElementById("emailErr").style.color="red";
        document.getElementById("emailErr").innerHTML="*E-mail id not valid";
        return false;
    }
    else{
        document.getElementById("emailErr").innerHTML="";
    }

    // Validating Password
    if(passWord==""){
        document.getElementById("passErr").style.color="red";
        document.getElementById("passErr").innerHTML="*This field is required";
        return false;
    }

    else if(passWord.length<6){
        document.getElementById("passErr").style.color="red";
        document.getElementById("passErr").innerHTML="*Password must contain atleast 6 characters";
        return false;
    }

    else{
        document.getElementById("passErr").innerHTML="";
    }

    // Validating Confirm Password
    if(confirmPassword==""){
        document.getElementById("confirmPassErr").style.color="red";
        document.getElementById("confirmPassErr").innerHTML="*This field is required";
        return false;
    }
    else if(confirmPassword!=passWord){
        document.getElementById("confirmPassErr").style.color="red";
        document.getElementById("confirmPassErr").innerHTML="*Passwords do not match!!";
        return false;
    }
    else{
        document.getElementById("confirmPassErr").innerHTML="";
    }

    // document.getElementById("fnameErr").innerHTML=firstName;
    // document.getElementById("lnameErr").innerHTML=lastName;
    // document.getElementById("genderDivErr").innerHTML= genderDivValue;
    // document.getElementById("contactErr").innerHTML=contactValue;
    // document.getElementById("dobErr").innerHTML=dayValue+"/"+monthValue+"/"+yearValue;
    // document.getElementById("emailErr").innerHTML=emailValue;
    // document.getElementById("passErr").innerHTML=passWord;
    // document.getElementById("confirmPassErr").innerHTML=confirmPassword;
    // return false;
    
}

