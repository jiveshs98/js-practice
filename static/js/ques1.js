
function formFun(){
    var firstName= document.getElementById("firstname").value;
    var lastName= document.getElementById("lastname").value;


    var revfName= Array.from(firstName).reverse();
    var revlName= Array.from(lastName).reverse();

    var tot= firstName.length + lastName.length;

    var para= document.createElement("P");
    para.innerHTML=revfName.join("")+" "+revlName.join("")+" "+tot;

    var res= document.getElementById("result");
    res.appendChild(para);

    return false;
}