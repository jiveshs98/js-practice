/*
// Question #1
//Create a JS Function. It has one parameter: an array, nums. It must iterate through the array performing one of the following actions on each element:
//If the element is even, multiply the element by 2.
//If the element is odd, multiply the element by 3.
//The function must then return the modified array.

function modifyArray(nums) {
    var res= nums.map(x => {if(x%2===0){
                                return x*=2;
                            }
                            else{
                                return x*=3;
                            }
                            });

    return res;
}

console.log(modifyArray([1,2,3,4,5,6]));
*/


/*
// Question #2
//Given a date string, "dateString", in the format MM/DD/YYYY, find and return the day name for that date. Each day name must be one of the following strings: Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, or Saturday. For example, the day name for the date 12/07/2016 is Wednesday.

function getDayName(dateString) {
    let dayName;
    // Write your code here

    // Split String into Array
    let dateStringArr= dateString.split('/');

    // Getting month, day and year
    let month= parseInt(dateStringArr[0])-1;
    let day= parseInt(dateStringArr[1]);
    let year= parseInt(dateStringArr[2]);

    // Passing above values in Date object
    let dateObj= new Date(year,month,day);

    // Creating Array of Day Name
    const days= ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];

    // Storing Day Name
    dayName= days[dateObj.getDay()];

    return dayName;
}

console.log(getDayName("12/07/2016"));
console.log(getDayName("10/11/2009"));

// Output
// Wednesday
// Sunday
*/