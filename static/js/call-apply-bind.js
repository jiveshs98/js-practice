var funObj={
    bioCall:function(name, id){
        document.write("<h1>Hi!!<em>This is a function stored in foo.</em></h1>");
        document.getElementById("p1").innerText="Hello, this is p tag.\nName: "+name+"\nID: "+id+"\nGender: "+this.gender+"\nCountry: "+this.country;
        },

    bioApply: function(name, id){
        document.write("<h1>Hi!!<em>This is a function stored in foo.</em></h1>");
        document.getElementById("p2").innerText="Hello, this is p tag.\nName: "+name+"\nID: "+id+"\nGender: "+this.gender+"\nCountry: "+this.country;
        }
}; 


var obj= {
    country: null,
    gender: null,
};


var empName= prompt("Please enter your name:");
var empId= prompt("Please enter your id:");
var empCountry= prompt("Please enter your country:");
obj.country= empCountry;
var empGender= prompt("Please enter your gender (Male/Female/Other):");
obj.gender= empGender;


var arr=[empName, empId];

// Call Method
document.write("<p><em>This is Call Method</em></p>");
funObj.bioCall.call(obj,empName,empId);

// Apply Method
document.write("<p><em>This is Apply Method</em></p>");
funObj.bioApply.apply(obj, arr);



// Implementing bind()

this.car= "Scorpio";

var markGarage={
    car: "Mercedes",
    getcar: function(){
        return this.car;
    }
};

var markCar= markGarage.getcar;

console.log(markCar());     //Output is Scorpio but it should be Mercedes

var realMarkCar= markGarage.getcar.bind(markGarage);

console.log(realMarkCar());
