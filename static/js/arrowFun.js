// Implementing arrow function in JS

// #1 Basic Implementation

//fun1() can't be used before it's definition otherwise the browser will not treat it as a function
//There should be no line break between () and =>
var fun1= () => {
    document.write("<br><br><strong>1. This is an Arrow function.</strong>");
};

fun1();


// #2 Arrow Function with parameter

document.write("<br><br><h4>2. Following are the Arrow functions with parameters:-</h4><br><br>");


// Arrow function with a single parameter can be given with or without ().
var fun2= (a) => {
    document.write("This is a function with a single parameter <strong>inside</strong> ().<br>"+a);
};

fun2(10);

var fun3= a => {
    document.write("<br><br>This is a function with a single parameter <strong>without</strong> ().<br>"+a);
};

fun3(30);

// However () is mandatory for arrow functions with multiple parameters.
var fun4= (a,b) => {
    document.write("<br><br>This is a function with a multiple parameters <strong>inside</strong> ().<br>"+a+", "+b);
};

fun4(50, 100);

// Arrow function with default parameter.
var fun5= (a,b=20) => {
    document.write("<br><br>This is a function with default parameter.<br>"+a+", "+b);
};
fun5(50);

// Arrow function with Rest parameter.
var fun6= (a, ...args) => {
    document.write("<br><br>This is a function with rest parameter.<br>"+a+" "+args);
};
fun6(10,50,40,30,20);

// #3 Single-line Arrow Function.
document.write("<br><br><strong>3. This is a single-line arrow function.</strong><br>");
var fun7= a => document.write(a);
document.write("<br>Lorem Ipsum Dolores Septum<br>");
fun7(2011);


// #4 Arrow function with a statement block. (Notice how the order of output changes even if the code written is almost same!!!)
document.write("<br><br><strong>4. This is an Arrow function with a statement block.</strong><br><br>");
var fun8= a => {document.write(a);
document.write("<br>Lorem Ipsum Dolores Septum<br>")};
fun8(2011);

// #5 Arrow function with a return statement.
var fun9= c =>{return c;};
document.write("<br><br><strong>5. This is arrow function with a <em>return</em> statement.</strong><br>"+fun9(123));

// #6 Arrow function without a return statement.
// Please note that "var fun10= c => {c};" will not work!!!
// #5 is equivalent to #6  

var fun10= c => c;  // Applicable for (a,b) => a+b; fun10(123,321)
document.write("<br><br><strong>6. This is arrow function without a <em>return</em> statement.</strong><br>"+fun10(321));

// P.S.: anonymousFun.js----> #2-#4 applicable for arrow function too.