// JS Exercise from w3Resource

/*

//Question #1

// Write a JavaScript program to display the current day and time in the following format.
// Sample Output ===> Today is : Tuesday.
//                   Current time is : 10 PM : 30 : 38


let date= new Date();
let Days= ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
let day= Days[date.getDay()];

let hr= date.getHours();
let amPm= (hr>12) ? "PM" : "AM";

if(hr>12);
hr-=12;

let minu= date.getMinutes();
let sec= date.getSeconds();
let time=hr+" "+amPm+": "+minu+": "+sec;

document.write("<br>Today is: "+day);
document.write("<br>Current Time is: "+time);

*/


/*
// Question #2

// Write a JavaScript program to print the contents of the current window.

console.log(window.print());    // Output: Dialog Box Opens to print the contents of the window; Console Output: undefined;

*/



/*
//Question #3

// Write a JavaScript program to get the current date.  Go to the editor
//Expected Output : mm-dd-yyyy, mm/dd/yyyy or dd-mm-yyyy, dd/mm/yyyy

var dateObj= new Date();
var date= dateObj.getDate();
var month= dateObj.getMonth();
var year= dateObj.getFullYear();

document.write("<h1>"+month+"-"+date+"-"+year+"</h1>");
document.write("<h1>"+month+"/"+date+"/"+year+"</h1>");
document.write("<h1>"+date+"-"+month+"-"+year+"</h1>");
document.write("<h1>"+date+"/"+month+"/"+year+"</h1>");
*/


/*
// Question #4

// Write a JavaScript program to find the area of a triangle where lengths of the three of its sides are 5, 6, 7.
// Area of triangle= (s(s-a)(s-b)(s-c))^1/2 ; where s= (a+b+c)/2

var side1= 5;
var side2= 6;
var side3= 7;

var s= (side1+side2+side3)/2;

var area= Math.sqrt(s*(s-side1)*(s-side2)*(s-side3));

document.write("<h1>The area of triangle is: "+area+"</h1>");
*/



/*
// Question #5

// Write a JavaScript program to rotate the string 'w3resource' in right direction by periodically removing one letter from the end of the string and attaching it to the front.

var string= 'Hello';
var n= 0;
console.log(string);
while(n<string.length){
    var temp="";
    temp+=string[string.length-1];
    
    for(var i=0; i<string.length-1;i++){
        temp+=string[i];
    }
    console.log(temp);
    string= temp;
    n= n+1;
    
    
}
*/



/*
// Question #6

// Write a JavaScript program to rotate the string 'w3resource' in left direction by periodically removing one letter from the start of the string and attaching it to the end.

var string= 'w3resource';
var n= 0;
console.log(string);
while(n<string.length){
    var temp="";
    
    
    for(var i=1; i<string.length;i++){
        temp+=string[i];
    }
    temp+=string[0];
    console.log(temp);
    string= temp;
    n= n+1;
        
}

*/


/*
// Question #7
//  Write a JavaScript program to determine whether a given year is a leap year in the Gregorian calendar. 

var year= prompt("Enter year:");
{
if(year%4==0){

    if(year%100==0){
        
        if(year%400==0)
            document.write("<h1>"+year+" is a leap year!!</h1>");
        

        else
            document.write("<h1>"+year+" is not a leap year!!</h1>");
        
    }

    else{
        document.write("<h1>"+year+" is a leap year!!</h1>");
    }
}

else{
    document.write("<h1>"+year+" is not a leap year!!</h1>");
}

}
*/


/*
// Question #8
// Write a JavaScript program where the program takes a random integer between 1 to 10, the user is then prompted to input a guess number. If the user input matches with guess number, the program will display a message "Good Work" otherwise display a message "Not matched".

function guessNum(x,y){
    if (x===y){
        return "Good Work";
    }

    else{
        return "Not matched";
    }
}

while(true){

    var randNum= Math.floor(Math.random()*10 + 1);

    var userNum= parseInt(prompt("Enter number (1-10): "));

    var answer= guessNum(randNum,userNum);

    //document.write("<h1>"+answer+"</h1>");

    

    var ch= prompt('Do you want to continue? (y/n)');

    if(ch==='y'||ch==='Y'){
        console.log(answer);
        continue;
    }
    else if(ch==='n'||ch==='N'){
        console.log(answer);
        break;
    }
    else{
        alert('Invalid answer!! Please try (y/n) or (Y/N).');
    }
}
*/


/*
// Question #9
//Write a JavaScript program to calculate multiplication and addition of two numbers (input from user).
// Create a div element with id='result' in html file



var form= document.createElement("form");
document.body.appendChild(form);

var inputNum1= document.createElement("input");
inputNum1.setAttribute("type","text");
inputNum1.setAttribute("name","inputNum1");
inputNum1.setAttribute("id","num1");
inputNum1.setAttribute("placeholder","Enter first operand");
form.appendChild(inputNum1);

var inputNum2= document.createElement("input");
inputNum2.setAttribute("type","text");
inputNum2.setAttribute("name","inputNum2");
inputNum2.setAttribute("id","num2");
inputNum2.setAttribute("placeholder","Enter second operand");
form.appendChild(inputNum2);

var buttonAdd= document.createElement("button");
buttonAdd.innerHTML="Add";
buttonAdd.setAttribute("onclick","funAdd()");
document.body.appendChild(buttonAdd);

var buttonMul= document.createElement("button");
buttonMul.innerHTML="Mul";
buttonMul.setAttribute("onclick","funMul()");
document.body.appendChild(buttonMul);

var resultDiv= document.createElement("div");
resultDiv.setAttribute("id","result");
document.body.appendChild(resultDiv);

function funAdd(){
    var num1= parseFloat(document.getElementById('num1').value);
    var num2= parseFloat(document.getElementById('num2').value);
    var para= document.createElement("p");
    var addNum= num1 + num2;
    para.innerHTML= "<h4>Result of Addition: "+addNum+"</h4>";
    document.getElementById("result").appendChild(para);
}

function funMul(){
    var num1= parseFloat(document.getElementById('num1').value);
    var num2= parseFloat(document.getElementById('num2').value);
    var para= document.createElement("p");
    var mulNum= num1 * num2;
    para.innerHTML= "<h4>Result of Multiplication: "+mulNum+"</h4>";
    document.getElementById("result").appendChild(para);
}

*/



/*
// Question #10
// Write a JavaScript program to convert temperatures to and from Celsius, Fahrenheit.
// [ Formula : c/5 = (f-32)/9 [ where c = temperature in Celsius and f = temperature in Fahrenheit ]

function tempToCelsius(){
    var temp= parseFloat(document.getElementById("tempInput").value);
    var c= ((temp-32)/9)*5;
    var paraCelsius= document.createElement("p");
    paraCelsius.innerHTML="<h4>Temperature in Celsius is: "+c+"</h4>";
    document.getElementById("result").appendChild(paraCelsius);

}

function tempToFahrenheit(){
    var temp= parseFloat(document.getElementById("tempInput").value);
    var f= ((temp*9)/5)+32;
    var paraFahrenheit= document.createElement("p");
    paraFahrenheit.innerHTML= "<h4>Temperature in Fahrenheit is: "+f+"</h4>";
    document.getElementById("result").appendChild(paraFahrenheit);

}

var tempForm= document.createElement("form");
document.body.appendChild(tempForm);



var tableElement= document.createElement("table");
tempForm.appendChild(tableElement);
var tableRow= document.createElement("tr");
tableElement.appendChild(tableRow);

var tableData1= document.createElement("td");
tableRow.appendChild(tableData1);
var tempInputLabel= document.createElement("label");
tempInputLabel.setAttribute("for","tempInput");
tempInputLabel.innerHTML= "Enter temperature value: "
tableData1.appendChild(tempInputLabel);


var tableData2= document.createElement("td");
tableRow.appendChild(tableData2);
var tempInput= document.createElement("input");
tempInput.setAttribute("id","tempInput");
tempInput.setAttribute("type","text");
tempInput.setAttribute("name","temp");
tableData2.appendChild(tempInput);


var celsiusButton= document.createElement("button");
celsiusButton.setAttribute("onclick","tempToCelsius()");
celsiusButton.innerHTML="Convert To Celsius";
document.body.appendChild(celsiusButton);


var fahrenheitButton= document.createElement("button");
fahrenheitButton.setAttribute("onclick","tempToFahrenheit()");
fahrenheitButton.innerHTML="Convert To Fahrenheit";
document.body.appendChild(fahrenheitButton);


var resultDiv= document.createElement("div");
resultDiv.setAttribute("id","result");
document.body.appendChild(resultDiv);
*/


/*
// Question #11
// Write a JavaScript program to get the website URL (loading page).

var formLogin= document.createElement("form");
document.body.appendChild(formLogin);

var formTable= document.createElement("table");
formLogin.appendChild(formTable);

for(var i=1;i<4;i++){
    var TableRow= document.createElement("tr");
    rowId= "tableRow"+i;
    TableRow.setAttribute("id",rowId);
    formTable.appendChild(TableRow);
    
    var TableData1= document.createElement("td");
    dataId= "tableData"+i+"-1";
    TableData1.setAttribute("id",dataId);
    TableRow.appendChild(TableData1);

    var TableData2= document.createElement("td");
    dataId= "tableData"+i+"-2";
    TableData2.setAttribute("id",dataId);
    TableRow.appendChild(TableData2);

    var TableData3= document.createElement("td");
    dataId= "tableData"+i+"-3";
    TableData3.setAttribute("id",dataId);
    TableRow.appendChild(TableData3);

    var TableData4= document.createElement("td");
    dataId= "tableData"+i+"-4";
    TableData4.setAttribute("id",dataId);
    TableRow.appendChild(TableData4);
}

var labelEmail= document.createElement("label");
labelEmail.setAttribute("for","inputEmail");
labelEmail.innerHTML="Email";
document.getElementById("tableData1-2").appendChild(labelEmail);

var inputEmail= document.createElement("input");
inputEmail.setAttribute("type","email");
inputEmail.setAttribute("id","inputEmail");
inputEmail.setAttribute("name","e_mail");
document.getElementById("tableData1-4").appendChild(inputEmail);


var labelPassword= document.createElement("label");
labelPassword.setAttribute("for","inputPassword");
labelPassword.innerHTML= "Password";
document.getElementById("tableData2-2").appendChild(labelPassword);

var inputPassword= document.createElement("input");
inputPassword.setAttribute("type","password");
inputPassword.setAttribute("id","inputPassword");
inputPassword.setAttribute("name","pass");
document.getElementById("tableData2-4").appendChild(inputPassword);

var submitButton= document.createElement("input");
submitButton.setAttribute("type","submit");
submitButton.setAttribute("value","Submit");
document.getElementById("tableData3-4").appendChild(submitButton);

var buttonURL= document.createElement("button");
buttonURL.setAttribute("id","buttonURL");
buttonURL.innerHTML= "Click Me for URL";
document.body.appendChild(buttonURL);

var buttonWindow= document.createElement("button");
buttonWindow.setAttribute("id","buttonWindow");
buttonWindow.innerHTML= "Click Me for opening MDN website in new tab.";
document.body.appendChild(buttonWindow);

var buttonWindowNew= document.createElement("button");
buttonWindowNew.setAttribute("id","buttonWindowNew");
buttonWindowNew.innerHTML= "Click Me for opening MDN website in new window.";
document.body.appendChild(buttonWindowNew);

var resultDiv= document.createElement("div");
resultDiv.setAttribute("id","result");
document.body.appendChild(resultDiv);



document.getElementById("buttonURL").onclick= function (){
    var stringURL= document.URL;
    var resPara= document.createElement("p");
    resPara.innerHTML= "<h4>"+stringURL+"</h4>";
    document.getElementById("result").appendChild(resPara);
};


document.getElementById("buttonWindowNew").onclick= function (){

    // Opens new browser window
    window.open("https://developer.mozilla.org/en-US/","","width=300, height=300");
};

document.getElementById("buttonWindow").onclick= function (){

    // Opens new tab
    window.open("https://developer.mozilla.org/en-US/","Hello World");
};

*/


/*
// Question #12
// Get all the unique elements in string

function uniqueInOrder(item){
    var temp=Array.from(new Set(item));     //ES6
    return temp;
}

var str= 'AAbbBBsdevZA'
var res="";
for(var i=0;i<str.length;i++){
    if(res.indexOf(str.charAt(i))==-1){
        res+= str[i];
    }
}
*/


/*
// Question #13
//  Write a JavaScript function that reverse a number.
//Example x = 32243;
//Expected Output : 34223

function revNum(num) {
    var rev = '';
    while (num > 0) {
        var temp = num % 10;
        rev += temp;
        num = parseInt(num / 10);
    }
    return parseInt(rev);
}

// Without Using JS Function
function revNum1(num) {
    var numStr = '';
    numStr += num;

    var rev = '';
    for (var i = numStr.length - 1; i >= 0; i--) {
        rev += numStr[i];
    }
    return rev;
}

// Using JS Function
function revNum2(num) {
    var temp = num.toString().split('').reverse().join('');
    return temp;
}

console.log(revNum(19988991178));
console.log(revNum(12345));
console.log(revNum1(32243));
console.log(revNum1(12345));
console.log(revNum2(32243));
console.log(revNum2(12345));
*/


/*
// Question #14
// Write a JavaScript function that checks whether a passed string is palindrome or not?
// A palindrome is word, phrase, or sequence that reads the same backward as forward, e.g., madam or nurses run.


// Without JS Function
function checkPalindrome(word) {
    var temp = word;
    var rev = '';
    for (var i = word.length - 1; i >= 0; i--) {
        rev += word[i];
    }
    if (temp == rev) {
        return word + " is a palindrome";
    }
    else {
        return word + " is not a palindrome";
    }
}


// With JS Function
function checkPalindrome2(word) {
    var rev = word.split("").reverse().join("");

    var result = rev == word ? " is a palindrome" : " is not a palindrome";

    return word + result;
}

console.log(checkPalindrome("madam"));
console.log(checkPalindrome("boy"));
console.log(checkPalindrome2("madam"));
console.log(checkPalindrome2("boy"));
*/


/*
// Question #15
// Write a JavaScript function that generates all combinations of a string.
// Example string : 'dog'
// Expected Output : d,do,dog,o,og,g


// With JS Function
function stringCombination(s) {
    arr= [];
    for (i = 0; i < s.length; i++) {
        for (j = i + 1; j < s.length + 1; j++) {
            arr.push(s.slice(i, j));
        }
    }
    return arr;
}


// Without JS Function      (Recommended)
function stringCombination2(s) {
    var result= "";
    result+=s;

    for(var k=0; k < s.length; k++){
        result+= " , "+s[k];   
    }

    for (var i = 0; i < s.length; i++) {
        for (var j = i+1; j < s.length; j++) {
            var sliceStr= s[i]+s[j];
            result+= " , "+sliceStr;
        }
    }

    return result;
}




document.write(stringCombination("dog"));
console.log(stringCombination("dog"));


console.log(stringCombination2("dog"));         // Output: dog , d , o , g , do , dg , og
console.log(stringCombination2("joker"));       // Output: joker , j , o , k , e , r , jo , jk , je , jr , ok , oe , or , ke , kr , er
*/


/*
// Question #16
// Write a JavaScript function that returns a passed string with letters in alphabetical order.
// Example string : 'webmaster'
// Expected Output : 'abeemrstw'
// Assume punctuation and numbers symbols are not included in the passed string.


// With JS Function
function sortStr(s) {
    return s.split("").sort().join("");
}


// Without JS Function
function sortStr2(s) {

    var arr = [];
    for (var j = 0; j < s.length; j++) {
        arr[arr.length] = s[j];
    }

    var result = "";
    for (var i = 0; i < arr.length; i++) {

        for (var k = 0; k < arr.length; k++) {

            if (arr[k] > arr[k + 1]) {
                var temp = arr[k];
                arr[k] = arr[k + 1];
                arr[k + 1] = temp;
            }
            else {
                continue;
            }
        }
    }

    for (var z = 0; z < arr.length; z++) {
        result += arr[z];
    }

    return result;
}

var stringInput = 'webmaster';
console.log(stringInput);               //webmaster
console.log(sortStr(stringInput));      //abeemrstw

var stringInput2 = 'huikopaswqebmt';    
console.log(stringInput2);              //huikopaswqebmt
console.log(sortStr2(stringInput2));    //abehikmopqstuw

var stringInput3 = 'jivesh';
console.log(stringInput3);              //jivesh
console.log(sortStr2(stringInput3));    //ehijsv

//Output
// webmaster
// abeemrstw
// huikopaswqebmt
// abehikmopqstuw
// jivesh
// ehijsv
*/


/*
// Question #17
// Write a JavaScript function that accepts a string as a parameter and converts the first letter of each word of the string in upper case.
// Example string : 'the quick brown fox'
// Expected Output : 'The Quick Brown Fox '


//  With JS Function
function upperFirst(s) {
    var arr = s.split(" ");
    var resArr = [];

    for (var i = 0; i < arr.length; i++) {
        resArr.push(arr[i].charAt(0).toUpperCase() + arr[i].slice(1));
    }

    return resArr.join(" ");
}


// Without JS Function........Method #1
function upperFirst2(s) {

    // Split the string separated by whitespaces
    var arr = [];
    var str = "";
    for (var j = 0; j <= s.length; j++) {
        if (j != s.length) {
            if (s[j] != " ") {
                str += s[j];
            }
            else {
                arr[arr.length] = str;
                str = "";
            }
        }
        else {
            arr[arr.length] = str;
        }
    }

    // Convert First Character into UpperCase
    var resArr = [];
    for (var i = 0; i < arr.length; i++) {
        var firstChar = arr[i][0].toUpperCase();
        var restChar = "";
        for (var z = 1; z < arr[i].length; z++) {
            restChar += arr[i][z];
        }
        resArr[resArr.length] = firstChar + restChar;
    }

    // Convert Array into String
    var result = "";
    result += resArr[0];
    for (var arrIndex = 1; arrIndex < resArr.length; arrIndex++) {
        result += " " + resArr[arrIndex];
    }
    return result;
}


// Without JS Function........Method #2
function upperFirst3(input) {

    var upper_case = input[0].toUpperCase();
    for (var i = 1; i <= input.length - 1; i++) {
        let currentChar, //current character
            previousChar = input[i - 1]; //previous character
        if (previousChar && previousChar == ' ') {
            currentChar = input[i].toUpperCase();
        } else {
            currentChar = input[i];
        }
        upper_case = upper_case + currentChar;
    }
    return upper_case;
}


console.log(upperFirst("the quick brown fox"));
console.log(upperFirst2("tit for tat"));
console.log(upperFirst2("the earth revolves around the sun"));
console.log(upperFirst3("this is the third method"));


// Output
// The Quick Brown Fox
// Tit For Tat
// The Earth Revolves Around The Sun
// This Is The Third Method
*/


/*
// Question #18
// Write a JavaScript function that accepts a string as a parameter and find the longest word within the string.
// Example string : 'Web Development Tutorial'
// Expected Output : 'Development'

// With JS Function
function longestWord(s){
    var arr=s.split(" ");
    var longestIndex=0;
    for(var i=0;i<arr.length;i++){
        if(arr[longestIndex].length<arr[i].length){
            longestIndex= i;
        }
        else{
            continue;
        }
    }
    return arr[longestIndex];
}

// Without JS Function
function longestWord2(s){
    // Split the string separated by whitespaces
    var arr = [];
    var str = "";
    for (var j = 0; j <= s.length; j++) {
        if (j != s.length) {
            if (s[j] != " ") {
                str += s[j];
            }
            else {
                arr[arr.length] = str;
                str = "";
            }
        }
        else {
            arr[arr.length] = str;
        }
    }

    // Find the longest word
    var longestIndex=0;
    for(var i=0;i<arr.length;i++){
        if(arr[longestIndex].length<arr[i].length){
            longestIndex= i;
        }
        else{
            continue;
        }
    }
    return arr[longestIndex];
}

console.log(longestWord("Web Development Tutorial"));
console.log(longestWord2("Centralized Facilities Helpdesk"));


//Output
//Development
//Centralized

*/

/*
// Question #19
// Write a JavaScript function that accepts a string as a parameter and counts the number of vowels within the string.
// Note : As the letter 'y' can be regarded as both a vowel and a consonant, we do not count 'y' as vowel here.
// Example string : 'The quick brown fox'
// Expected Output : 5

// With JS Function Method #1
function findVowel(s){
    var stringVowel= "aeiouAEIOU";
    var count= 0;

    for(var i=0;i<s.length;i++){
        if(stringVowel.indexOf(s[i]) !== -1){
            count++;
        }
        else{
            continue;
        }
    }

    return count;
}

// With JS Function Method #2
function FindVowel(s){
    //debugger
    var stringVowel= "aeiouAEIOU";
    var count= 0;

    var stringArr= s.split("");
    for(var i=0; i<stringArr.length; i++){

        for(var j=0; j<stringVowel.length; j++){
            if(stringArr[i] === stringVowel[j] ){
                count++;
            }
            else{
                continue;
            }
        }
        
    }

    return count;
}

// Without JS Function Method #1
function findVowel2(s){
    var arrVowel=['a','e','i','o','u','A','E','I','O','U'];
    var count= 0;

    for(var j=0;j<s.length;j++){
        for(var x in arrVowel){
            if(s[j] === arrVowel[x]){
                count++;
            }
            else{
                continue;
            }
        }
    }

    return count;
}

// Without JS Function Method #2
function FindVowel2(s){
    var arrVowel=['a','e','i','o','u','A','E','I','O','U'];
    var count= 0;
    var stringArr=[];

    // Splitting the string into Array
    for(var k=0; k<s.length;k++){
        stringArr[stringArr.length]= s[k];
    }

    // Counting the no. of vowels
    for(var j=0;j<stringArr.length;j++){
        for(var x in arrVowel){
            if(stringArr[j] === arrVowel[x]){
                count++;
            }
            else{
                continue;
            }
        }
    }

    return count;
}

var inputString= "ShE sells sea-shells On the SeA-Shore";
console.log(findVowel(inputString));
console.log(FindVowel("Jivesh Singh"));

console.log(findVowel2('The quick brown fox'));
console.log(findVowel2('ThE qUick brOwn fOx'));
console.log(FindVowel2('Jivesh Singh'));


// Output
// 11
// 3
// 5
// 5
// 3
*/


/*
// Question #20
// Write a JavaScript function that accepts a number as a parameter and check the number is prime or not.
// Note : A prime number (or a prime) is a natural number greater than 1 that has no positive divisors other than 1 and itself.

function checkPrime(num){
    if(num === 1){
        return "Not a Prime number";
    }
    
    else{
        for(var x=2;x<num;x++){
            
            if(num%x === 0){
                return "Not a Prime number";
            }
            else{
                continue;
            }
        }
    }

    return "a Prime number";
}


var inputNum= 23;
var inputNum2= 7;
var inputNum3= 8; 

console.log(inputNum+" is "+checkPrime(inputNum));
console.log(inputNum2+" is "+checkPrime(inputNum2));
console.log(inputNum3+" is "+checkPrime(inputNum3));


//Output
//23 is a Prime number
//7 is a Prime number
//8 is Not a Prime number
*/


/*
// Question #21
//Write a JavaScript function which will take an array of numbers stored and find the second lowest and second greatest numbers, respectively.
//Sample array : [1,2,3,4,5]
//Expected Output : 2,4

// With JS Function
function secLowGreat(arr){
    var sortedArr= arr.sort(function(a,b){return a-b});
    var result= [];
    var secLowest= sortedArr[1];
    result.push(secLowest);
    var secGreatest= sortedArr[sortedArr.length-2];
    result.push(secGreatest);

    return result.join(",");
} 

// Without JS Function
function secLowGreat2(arr){
    
        // Sorting Array
        var sortedArr = [];
        for (var i = 0; i < arr.length; i++) {
    
            for (var k = 0; k < arr.length; k++) {
    
                if (arr[k] > arr[k + 1]) {
                    var temp = arr[k];
                    arr[k] = arr[k + 1];
                    arr[k + 1] = temp;
                }
                else {
                    continue;
                }
            }
        }
    
        for (var z = 0; z < arr.length; z++) {
            sortedArr[sortedArr.length]= arr[z];
        }
        
        // Find the second Lowest and Second Greatest
        var result= "";
        var secLowest= sortedArr[1];
        result+=secLowest;
        var secGreatest= sortedArr[sortedArr.length-2];
        result+= ","+secGreatest;
        return result;
    
}

var inputArr= [4,2,6,9,84,21];
console.log(secLowGreat(inputArr));
var inputArr2= [1,2,3,4,5];
console.log(secLowGreat2(inputArr2));
*/


/*
// Question #22
// Write a JavaScript function which says whether a number is perfect.
// According to Wikipedia : In number theory, a perfect number is a positive integer that is equal to the sum of its proper positive divisors, that is, the sum of its positive divisors excluding the number itself (also known as its aliquot sum). Equivalently, a perfect number is a number that is half the sum of all of its positive divisors (including itself).
// Example : The first perfect number is 6, because 1, 2, and 3 are its proper positive divisors, and 1 + 2 + 3 = 6. Equivalently, the number 6 is equal to half the sum of all its positive divisors: ( 1 + 2 + 3 + 6 ) / 2 = 6. The next perfect number is 28 = 1 + 2 + 4 + 7 + 14. This is followed by the perfect numbers 496 and 8128.

function checkPerfect(num)
{
var temp = 0;
   for(var i=1;i<=num/2;i++)
     {
         if(num%i === 0)
          {
            temp += i;
          }
     }
   
     if(temp === num && temp !== 0)
        {
            return " is a perfect number.";
        } 
     else
        {
            return " is not a perfect number.";
        }   
} 

var inputNum= 28;
console.log(inputNum+checkPerfect(inputNum));

var inputNum2= 30;
console.log(inputNum2+checkPerfect(inputNum2));

//Output
// 28 is a perfect number.
// 30 is not a perfect number.
*/


/*
// Question #23
// Write a JavaScript function to compute the factors of a positive integer.
// Input: 15
// Output: [1,3,5,15]


// With JS Function
function factors(num){
    result=[];
    for(var i=1 ; i<=num ; i++){
        if(num%i === 0){
            result.push(i);
        }

        else{
            continue;
        }
    }

    return result;
}


// Without JS Function
function factors2(num){
    result=[];
    for(var i=1 ; i<=num ; i++){
        if(num%i === 0){
            result[result.length]= i;
        }

        else{
            continue;
        }
    }

    return result;
}

var inputNum= 15;
var inputNum2= 17;

console.log(factors(inputNum));
console.log(factors(inputNum2));

console.log(factors2(inputNum));
console.log(factors2(inputNum2));


//Output
//(4) [1, 3, 5, 15]
//(2) [1, 17]
//(4) [1, 3, 5, 15]
//(2) [1, 17]
*/


/*
//Question #24
//Write a JavaScript function to compute the value of b^n where n is the exponent and b is the bases. Accept b and n from the user and display the result.

var form= document.createElement("form");
document.body.appendChild(form);


var inputTable= document.createElement("table");
form.appendChild(inputTable);

for(var i=1; i<3; i++){
    var inputTableRow= document.createElement("tr");
    var idRow= "tableRow-"+i;
    inputTableRow.setAttribute("id",idRow);
    inputTable.appendChild(inputTableRow);

    var inputTableData1= document.createElement("td");
    var idRowData1= "tableData-"+i+"-1";
    inputTableData1.setAttribute("id",idRowData1);
    inputTableRow.appendChild(inputTableData1);

    var inputTableData2= document.createElement("td");
    var idRowData2= "tableData-"+i+"-2";
    inputTableData2.setAttribute("id",idRowData2);
    inputTableRow.appendChild(inputTableData2);
}

var inputBaseLabel= document.createElement("label");
inputBaseLabel.setAttribute("for","inputBase");
inputBaseLabel.innerHTML= "Enter Base Value: ";
document.getElementById("tableData-1-1").appendChild(inputBaseLabel);

var inputBase= document.createElement("input");
inputBase.setAttribute("id","inputBase");
inputBase.setAttribute("type","text");
inputBase.setAttribute("name","base");
document.getElementById("tableData-1-2").appendChild(inputBase);

var inputExponentLabel= document.createElement("label");
inputExponentLabel.setAttribute("for","inputExponent");
inputExponentLabel.innerHTML= "Enter Exponent Value: ";
document.getElementById("tableData-2-1").appendChild(inputExponentLabel);

var inputExponent= document.createElement("input");
inputExponent.setAttribute("id","inputExponent");
inputExponent.setAttribute("type","text");
inputExponent.setAttribute("name","exponent");
document.getElementById("tableData-2-2").appendChild(inputExponent);

var calcButton= document.createElement("button");
calcButton.setAttribute("id","calcButton");
calcButton.innerHTML= "Calculate";
document.body.appendChild(calcButton);

var resPara= document.createElement("p");
resPara.setAttribute("id","result");
document.body.appendChild(resPara);

document.getElementById("calcButton").onclick= function (){
    var base= parseInt(document.getElementById("inputBase").value);
    var exponent= parseInt(document.getElementById("inputExponent").value);
    var res= base**exponent;
    document.getElementById("result").innerHTML= "<h3>The result is: "+res+"</h3>";
};
*/


/*
// Question #25
//Write a JavaScript function to extract unique characters from a string.
//Example string : "thequickbrownfoxjumpsoverthelazydog"
//Expected Output : "thequickbrownfxjmpsvlazydg"

// With JS Function
function extractUnique(s) {
    var uniqueString = "";
    for (var i = 0; i < s.length; i++) {
        if (uniqueString.indexOf(s.charAt(i)) === -1) {
            uniqueString += s[i];
        }
        else {
            continue;
        }
    }
    return uniqueString;
}

// Without JS Function
function extractUnique2(s) {
    var uniqueString = '';

    for (var i = 0; i < s.length; i++) {
        var flag = false;
        for (var j = 0; j < uniqueString.length; j++) {

            if (s[i] == uniqueString[j]) {

                flag = true;
                break;
            }

        }

        if (!flag) {
            uniqueString += s[i];
        }

    }

    return uniqueString;
}

console.log(extractUnique("thequickbrownfoxjumpsoverthelazydog"));
console.log(extractUnique2("theearthisround"));

// Output
//thequickbrownfxjmpsvlazydg
//thearisound
*/


/*
// Question #26
// Write a JavaScript function to  get the number of occurrences of each letter in specified string.

// Method #1
function countChar(s) {
    var uniqueChar = {};
    s.replace(/\S/g, function(c){uniqueChar[c] = (isNaN(uniqueChar[c]) ? 1 : uniqueChar[c] + 1);});
    return uniqueChar;
}

// Method #2
function countChar2(s){
    var uniqueChar={};
    s = s.replace(/[^a-z0-9]/gi, '');
    
    for(x = 0; x < s.length; x++) {
        var c = s.charAt(x)
        uniqueChar[c] = (isNaN(uniqueChar[c]) ? 1 : uniqueChar[c] + 1);
    }
    return uniqueChar;
}

console.log(countChar("The quick brown fox jumps over the lazy dog"));
console.log(countChar2("The earth is round"));
*/


/*
// Question #27
// Write a JavaScript function that returns array elements larger than a given number.


// With JS Function Method #1
// arr is array, num is number to compare
function arrayLarge(arr,num){
    return arr.filter(x => x>num);
}

// With JS Function Method #2
function ArrayLarge(arr,num){

    var resultArr= [];

    for(var x in arr){
        if(arr[x]>num){
            resultArr.push(arr[x]);
        }
        else{
            continue;
        }
    }

    return resultArr;
}

// Without JS Function Method #1
function arrayLarge2(arr,num){
    
    var resultArr=[];

    for(var x in arr){
        if(arr[x]>num){
            resultArr[resultArr.length]= arr[x];
        }
    }

    return resultArr;
}


var inputArray= [1,2,11,15,6,78,4,25,39,34,18];
var inputNum= 25;

var InputArray= [23,56,48,59,11,55,2,6,7,98,101,52,69];
var InputNum= 70;

var inputArray2= [1,2,11,15,6,78,4,25,39,34,18];
var inputNum2= 40;


console.log(arrayLarge(inputArray,inputNum));
console.log(ArrayLarge(InputArray, InputNum));

console.log(arrayLarge2(inputArray2, inputNum2));


// Output
//(3) [78, 39, 34]
//(2) [98, 101]
//[78]
*/


/*
// Question #28
// Write a JavaScript function that generates a string id (specified length) of random characters.

// With JS Function without User Input (with charAt)
function randomString(num){
    var result='';
    var charString= "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*+-_<>,.?";
        
    for(var i=0;i<num;i++){
        var randomIndex= Math.floor(Math.random()*charString.length);
        result+= charString.charAt(randomIndex);
    }

    return result;
}

// With JS Function without User Input (without charAt)
function RandomString(num){
    var result='';
    var charString= "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*+-_<>,.?";
        
    for(var i=0;i<num;i++){
        var randomIndex= Math.floor(Math.random()*charString.length);
        result+= charString[randomIndex];
    }

    return result;
}

// With JS Function with User Input (with charAt)
function randomStringUser(num){
    var numUser= parseInt(num);
    var result='';
    var charString= "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*+-_<>,.?";
        
    for(var i=0;i<numUser;i++){
        var randomIndex= Math.floor(Math.random()*charString.length);
        result+= charString.charAt(randomIndex);
    }

    return result;
}

// With JS Function with User Input (without charAt)
function RandomStringUser(num){
    var numUser= parseInt(num);
    var result='';
    var charString= "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*+-_<>,.?";
        
    for(var i=0;i<numUser;i++){
        var randomIndex= Math.floor(Math.random()*charString.length);
        result+= charString[randomIndex];
    }

    return result;
}

// Without JS
// Math will still be used for random length of output string
function randomString2(num){
    var result='';
    var charString= "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*+-_<>,.?";
    var charList=[];
    for(var x in charString){
        charList[charList.length]= charString[x];
    }

    for(j=0;j<num;j++){
        var randomIndex= Math.floor(Math.random()*charList.length);
        result+= charString[randomIndex];
    }

    return result;
}

var inputLength= 5;
console.log(randomString(inputLength));

var InputLength= 10;
console.log(randomString(InputLength));

var inputLengthUser= prompt("Enter Length of String (with charAt): ");      //Input: 2
console.log(randomStringUser(inputLengthUser));

var InputLengthUser= prompt("Enter Length of String (without charAt): ");   //Input: 3
console.log(RandomStringUser(InputLengthUser));

var inputLength2= 7;
console.log(randomString2(inputLength2));


// Output
// 20Gbc
// mymp&ykYC1
// Lv
// Xve
// kyLRIBD

*/

// Question #29
// Write a JavaScript function to get all possible subset with a fixed length (for example 2) combinations in an array.
// Sample array : [1, 2, 3] and subset length is 2
// Expected output : [[2, 1], [3, 1], [3, 2], [3, 2, 1]]


// function subset(arra, arra_size)
//  {
//     var result_set = [], 
//         result;
    
   
// for(var x = 0; x < Math.pow(2, arra.length); x++)
//   {
//     result = [];
//     i = arra.length - 1; 
//      do
//       {
//       if( (x & (1 << i)) !== 0)
//           {
//              result.push(arra[i]);
//            }
//         }  while(i--);

//     if( result.length >= arra_size)
//        {
//           result_set.push(result);
//         }
//     }

//     return result_set; 
// }

// console.log(subset([1, 2, 3], 2));

/*
// Question #30
// Write a JavaScript function that accepts two arguments, a string and a letter and the function will count the number of occurrences of the specified letter within the string.
// Sample arguments : 'w3resource.com', 'o'
// Expected output : 2

// With JS Function
function countLetter(s,c){
    var charArray= s.split("");
    var count= 0;

    for(var x in charArray){
        if(charArray[x] === c){
            count++;
        }

        else{
            continue;
        }
    }

    return count;
}


// Without JS Function
function CountLetter(s,c){
    var charArray=[];
    var count= 0;

    // Splitting the string into array
    for(var x in s){
        charArray[charArray.length]= s[x];
    }

    // Counting the Characters
    for(var i=0; i<charArray.length; i++){
        if(c === charArray[i]){
            count++;
        }
    }

    return count;

}

var inputString= "w3resource.com";
var inputChar= "o";

console.log(countLetter(inputString, inputChar));


var inputString2= "My name is Shruti";
var inputChar2= "s";

console.log(CountLetter(inputString2,inputChar2));

// Output
// 2
// 1
*/


/*
// Question #31
// Write a JavaScript function to find the first not repeated character.
// Sample arguments : 'abacddbec'
// Expected output : 'e'

// With JS Function
function firstNotRepeat(s){
    var arr= s.split("");
    var count= 0;

    for(var i=0; i<s.length; i++){
        for(var a in arr){
            if(arr[a] === s.charAt(i)){
                count++;
            }

        }

        if(count == 1){
            return s.charAt(i);
        }

        else{
            count= 0;
        }
    }

    
}

// Without JS Function
function firstNotRepeat2(s){
    var str= s;
    var count= 0;

    for(var j=0; j<s.length; j++){
        for(var x in str){
            if(s[j] === str[x]){
                count++;
            }
        }

        if(count==1){
            return str[j];
        }

        else{
            count=0;
        }
    }
}


console.log(firstNotRepeat("abacddbec"));
console.log(firstNotRepeat("shrutigupta"));

console.log(firstNotRepeat2("abacddbec"));
console.log(firstNotRepeat2("shrutigupta"));

// Output
// e
// s
// e
// s
*/


/*
// Question #32
// Find longest substring in a given a string without repeating characters

function longest_substring_without_repeating_characters(input) {
    var chars = input.split('');
    var curr_char;
    var str = "";
    var longest_string = "";
    var hash = {};
    for (var i = 0; i < chars.length; i++) {
    curr_char = chars[i];
    if (!hash[chars[i]]) 
    { 
    str += curr_char; 
    hash[chars[i]] = {index:i};
    }
    else 
    {
    if(longest_string.length <= str.length)
    {
    longest_string = str;
    }
    var prev_dupeIndex = hash[curr_char].index;
    var str_FromPrevDupe = input.substring(prev_dupeIndex + 1, i);
    str = str_FromPrevDupe + curr_char;
    hash = {};
    for (var j = prev_dupeIndex + 1; j <= i; j++) {
    hash[input.charAt(j)] = {index:j};
    }
    }
    }
    return longest_string.length > str.length ? longest_string : str;
}
console.log(longest_substring_without_repeating_characters("google.com")); 
    
console.log(longest_substring_without_repeating_characters("example.com")); 

//Output
// gle.com
// xample.co
*/


/*
// Question #33
// Write a JavaScript function that returns the longest palindrome in a given string.
function is_Palindrome(str1) {
    var rev = str1.split("").reverse().join("");
    return str1 == rev;
    }
    
    function longest_palindrome(str1){
    
    var max_length = 0,
    maxp = '';
    
    for(var i=0; i < str1.length; i++) 
    {
    var subs = str1.substr(i, str1.length);
    
    for(var j=subs.length; j>=0; j--) 
    {
    var sub_subs_str = subs.substr(0, j);
    if (sub_subs_str.length <= 1)
    continue;
    
    if (is_Palindrome(sub_subs_str))
    {
    if (sub_subs_str.length > max_length) 
    {
    max_length = sub_subs_str.length;
    maxp = sub_subs_str;
    }
    }
    }
    }
    
    return maxp;
}
console.log(longest_palindrome("abracadabra"));
    
console.log(longest_palindrome("HYTBCABADEFGHABCDEDCBAGHTFYW12345678987654321ZWETYGDE")); 

// Output
// aca
// 12345678987654321
*/



/*
// Question #34
//  Write a JavaScript function to split a string and convert it into an array of words.


// With JS Function
function stringToArray(s){
    return s.split(" ");
}

// Without JS Function
function stringToArray2(s){
    var arr = [];
    var str = "";
    for (var j = 0; j <= s.length; j++) {
        if (j != s.length) {
            if (s[j] != " ") {
                str += s[j];
            }
            else {
                arr[arr.length] = str;
                str = "";
            }
        }
        else {
            arr[arr.length] = str;
        }
    }

    return arr;
}

console.log(stringToArray("Robin Singh"));
console.log(stringToArray2("within Amazon offices"));


// Output
//(2) ["Robin", "Singh"]
//(3) ["within", "Amazon", "offices"]

*/



/*
// Question #35
// Write a JavaScript function to extract a specified number of characters from a string.

// With JS Function
function stringExtract(s,length){
    result='';
    for(var i=0; i<=length-1; i++){
        result+= s.charAt(i);
    }
    return result;
}


// Without JS Function
function stringExtract2(s, length){
    result='';
    for(var j=0; j<=length-1; j++){
        result+= s[j];
    }
    return result;
}

var str= "Robin Singh";
var num= "4";
console.log(stringExtract(str, num));

var str2= "w3Resource";
var num2= "4";
console.log(stringExtract2(str2, num2));

// Output
// Robi
// w3Re
*/


/*
// Question #36
// Write a JS function to calculate factorial of a given number

function factorial(n){
    var result= 1;
    if(n === 0){
        return result;
    }
    else if(n < 0){
        result= undefined;
        return undefined;
    }
    else{
      for(var i=2; i<=n; i++){
        result*=i;
      }
      return result;
    }
}

var num= 5;
var num2= 0;
var num3 = -4;
var num4 = 3;

console.log("Factorial of "+num+" is: "+factorial(num));
console.log("Factorial of "+num2+" is: "+factorial(num2));
console.log("Factorial of "+num3+" is: "+factorial(num3));
console.log("Factorial of "+num4+" is: "+factorial(num4));


// Output
// Factorial of 5 is: 120
// Factorial of 0 is: 1
// Factorial of -4 is: undefined
// Factorial of 3 is: 6
*/



// Conditionals

/*
// Question #1
// Write a JavaScript program that accept two integers and display the larger.

function larger(a,b){
    if(a>b){
        return a+" is larger";
    }
    else if(a<b){
        return b+" is larger";
    }
    else{
        return "Both are equal";
    }
}

console.log(larger(12,45));
console.log(larger(69,60));
console.log(larger(50,50));

// Output
// 45 is larger
// 69 is larger
// Both are equal
*/


/*
// Question #2
// Write a JavaScript conditional statement to find the sign of product of three numbers. Display an alert box with the specified sign.
// Sample numbers : 3, -7, 2
// Output : The sign is -

function specifySign(arr){
    var x= arr[0];
    var y= arr[1];
    var z= arr[2];

    var product= x*y*z;

    if(product>0){
        alert('The sign is +');
    }
    
    else if(product<0){
        alert('The sign is -');
    }

    else{
        alert('The product is 0');
    }
}

specifySign([1,-8,19]);
specifySign([0,0,19]);
specifySign([1,-8,19]);

//Output
// (Alert Boxes)
// The sign is -
// The product is 0
// The sign is -
*/