arr=[45,42,589,228,478,777,256,772];

function max(x){
    maxNum= x[0];
    //flag= 0
    for(var i=0;i<x.length;i++){
        if(maxNum<x[i]){
            maxNum=x[i];
            //flag=i;
        }
        else{
            continue;
        }
    }

    return maxNum;
}

//main function
function maxSecond(myArray) {
    var x= max(myArray)
    //storing index of x value in y
    var y= myArray.indexOf(x)
    //removing x value with a index of y using splice()
    myArray.splice(y,1)
    //now again max of left over elements in array
    //gives second largest number
    var z = max(myArray);
    return z;
}



document.getElementById("result").innerHTML=maxSecond(arr);

/*

// Without using JS function

var myArray = [25,125,156,148];
var biggest = myArray[0];
var nextbiggest = myArray[0];
for(var i=0;i<myArray.length;i++){
if(myArray[i]>biggest){
nextbiggest = biggest;
biggest = myArray[i];
}
else if(myArray[i]>nextbiggest && myArray[i]!=biggest)
nextbiggest = myArray[i];
}

console.log(nextbiggest);

*/