// Calculate the average of scores of three teams and choose the winner

var john_average,mike_average,mary_average;

// John's team
var john_score1,john_score2,john_score3;

john_score1= 89;
john_score2= 120;
john_score3= 103;

john_average= (john_score1 + john_score2 + john_score3)/3;
console.log("Average of John's team: " + john_average);


// Mike's team
var mike_score1,mike_score2,mike_score3;

mike_score1= 116;
mike_score2= 94;
mike_score3= 123;

mike_average= (mike_score1 + mike_score2 + mike_score3)/3;
console.log("Average of Mike's team: " + mike_average);

//Mary's team
var mary_score1,mary_score2,mary_score3;

mary_score1= 97;
mary_score2= 134;
mary_score3= 105;

mary_average= (mary_score1 + mary_score2 + mary_score3)/3;
console.log("Average of Mary's team: " + mary_average);

if(john_average > mike_average &&  mary_average){
    console.log("Winner is John's team!!");
    console.log("Average Score: " + john_average);
}

else if(mike_average > john_average && mike_average > mary_average){
    console.log("Winner is Mike's team!!");
    console.log("Average Score: " + mike_average);
}

else if(mary_average>john_average && mary_average>mike_average){
    console.log("Winner is John's team!!");
    console.log("Average Score: " + john_average);
}

else{
    console.log("There is a draw!!");
}