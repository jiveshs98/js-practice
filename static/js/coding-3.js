// Tip 20% of the bill if the bill is less than 50, 15% if bill is between 50 and 200, 10% if bill is more than 200

function tipCalculator(bill) {

    var tip, amount;

    if (bill < 50) {
        tip = bill * 0.2;
        amount = bill + tip;


    }
    else if (50 <= bill && bill < 200) {
        tip = bill * 0.15;
        amount = bill + tip;

    }
    else {
        tip = bill * 0.1;
        amount = bill + tip;

    }


    return amount;



}

var arr1 = [124, 48, 268];
var result = [];
for (x in arr1) {
    console.log(arr1[x]);
    var temp = tipCalculator(arr1[x]);
    result.push(temp);
}

console.log(result);